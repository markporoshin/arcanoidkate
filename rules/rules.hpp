//
// Created by Екатерина Заболотских on 10/06/2019.
//



#ifndef ARCANOID_RULES_HPP
#define ARCANOID_RULES_HPP



#pragma once



#include <SFML/Window/Keyboard.hpp>
#include "../entity/circle.hpp"

class GameRuler;

class Rule {
public:
    std::string ruleName;

    virtual void rule(GameRuler * gr) = 0;
    virtual ~Rule() {};
};

class CircleRules : public Rule {
public:
    void rule(GameRuler * gr) override;
    ~CircleRules() override {}
};

class PlayerRules : public Rule {
public:
    int lives;
    PlayerRules(int lives) : lives(lives) {}
    void rule(GameRuler * gr) override;
    ~PlayerRules() override {}
};

class MapRules : public Rule {
    void rule(GameRuler * gr) override;
    ~MapRules() override {}
};

class SimpleRule : public Rule {
public:
    void rule(GameRuler *gr) override;

    ~SimpleRule() override;
};

class AddCircleRule : public Rule {
public:
    void rule(GameRuler *gr) override;
};

class SpeedUpRule : public Rule {
    void rule(GameRuler *gr) override;
};

class SizeUpRule : public Rule {
    void rule(GameRuler *gr) override;
};

class TrampolineRule : public Rule {
    void rule(GameRuler *gr) override;
};

class AddLiveRule : public Rule {
    void rule(GameRuler *gr) override;
};

class StartRule : public Rule {
    int ind; //num of controlled circle
public:
    StartRule(int ind, std::string name = "start");
    void rule(GameRuler *gr);

    ~StartRule() {};
};
#endif //ARCANOID_RULES_HPP
