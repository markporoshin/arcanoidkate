//
// Created by Екатерина Заболотских on 10/06/2019.
//

#include <cmath>
#include <iostream>
#include "rules.hpp"
#include "../entity/circle.hpp"
#include "../entity/player.hpp"
#include "../entity/brick.hpp"
#include "../entity/map.hpp"
#include "../entity/bonus.hpp"
#include "../defenitions.hpp"
#include "../game/GameRuler.hpp"


using namespace std;
using namespace sf;


//class GameRuler {
//public:
//    Vector2f winSize;
//
//    Map * map;
//    Player * player;
//    vector<Circle *> circles;
//    vector<Bonus *> bonuses;
//    std::map<string, Rule *> rules;
//
//    GameRuler(Vector2f winSize);
//
//    void update(float delta);
//
//    void endGame();
//
//    ~GameRuler();
//};


typedef enum {NONE = 0, DOWN, UP, RIGHT, LEFT} side;
static bool isPointInsideRect(Vector2f point, Vector2f rCenter, Vector2f rSide) {
    if (point.x < rCenter.x + rSide.x / 2 && point.x > rCenter.x - rSide.x / 2) {
        if (point.y < rCenter.y + rSide.y / 2 && point.y > rCenter.y - rSide.y / 2) {
            return true;
        }
    }
    return false;
}
static bool isPointInsizeCircle(Vector2f point, Vector2f cCenter, float R) {
    return sqrt((point.x - cCenter.x) * (point.x - cCenter.x) + (point.y - cCenter.y) * (point.y - cCenter.y)) < R;
}


static side isCollisionBonusPlayer(Bonus * c, Player * p, GameRuler *gr) {

    if (fabs(c->pos.x - p->pos.x) < (c->R + p->size.x / 2) &&
        fabs(c->pos.y - p->pos.y) < (c->R + p->size.y / 2)) {
        if (p->pos.x - p->size.x / 2 < c->pos.x &&
            p->pos.x + p->size.x / 2 > c->pos.x) {
            return DOWN;
        }
        return LEFT;
    }
    return NONE;

}
static side isCollisionCirclePlayer(Circle * c, Player * p, GameRuler *gr) {

    if (fabs(c->pos.x - p->pos.x) < (c->R + p->size.x / 2) &&
        fabs(c->pos.y - p->pos.y) < (c->R + p->size.y / 2)) {
        if (p->pos.x - p->size.x / 2 < c->pos.x &&
            p->pos.x + p->size.x / 2 > c->pos.x) {
            return DOWN;
        }
        return LEFT;
    }
    return NONE;

}
static side isCollisionCircleBrick(Circle * c, Brick * b, GameRuler * gr) {
    if (fabs(c->pos.x - b->pos.x) < (c->R + b->size.x / 2) &&
        fabs(c->pos.y - b->pos.y) < (c->R + b->size.y / 2)) {
        if (b->pos.x - b->size.x / 2 < c->pos.x &&
            b->pos.x + b->size.x / 2 > c->pos.x) {
            return DOWN;
        }
        return LEFT;
    }
    return NONE;
}
static pair<side, Brick *> isCollisionCircleMap(Circle * c, Map * m, GameRuler *gr) {
    side state;
    for (Brick * b : m->map)
        if(b->alive && (state = isCollisionCircleBrick(c, b, gr)))
            return {state, b};
    return {NONE, nullptr};
}
static side isCollisionCircleBorder(Circle * c, GameRuler *gr) {
    if ((c->pos.x + c->R) >= gr->winSize.x)
        return RIGHT;
    if ((c->pos.x - c->R) <= 0)
        return LEFT;
    if ((c->pos.y + c->R) > gr->winSize.y)
        return DOWN;
    if ((c->pos.y - c->R) <= 0)
        return UP;
    return NONE;
}
void CircleRules::rule(GameRuler *gr) {
    for (auto * c1 : gr->circles) {
        for (auto * c2 : gr->circles) {
            if (c1 != c2) {
                if (sqrt( (c1->pos.x - c2->pos.x) * (c1->pos.x - c2->pos.x) +
                                  (c1->pos.y - c2->pos.y) * (c1->pos.y - c2->pos.y)) < c1->R + c2->R) {
                    auto buf = c1->speed;
                    c1->speed = c2->speed;
                    c2->speed = buf;

                    c1->response(SIMPLE_DELTA);
                    c2->response(SIMPLE_DELTA);
                }
            }
        }
    }

    for (Circle * c : gr->circles) {
        side state = NONE;
        side buf;

        if((buf = isCollisionCirclePlayer(c, gr->player, gr)) != NONE)
            state = buf;

        pair<side, Brick *> resp = isCollisionCircleMap(c, gr->map, gr);
        if (resp.first) {
            gr->score += resp.second->cost;
            state = resp.first;
            Bonus * candidate = resp.second->handleHit();
            if (candidate != nullptr) {
                candidate->free();
                gr->bonuses.push_back(candidate);
            }
        }

        if((buf = isCollisionCircleBorder(c, gr)) != NONE) {
            state = buf;
            if (state == DOWN) {
                if(((PlayerRules *)gr->rules["player"])->lives--) {
                    Vector2f winSize = gr->winSize;
                    std::string n = std::to_string(rand() % RAND_MAX);
                    gr->circles.push_back(new Circle(CIRCLE_START_PARAM));
                    gr->rules.insert(make_pair(n, new StartRule(gr->circles.size()-1, n)));
                }
                gr->circles.erase(std::find(gr->circles.begin(), gr->circles.end(), c));
                delete c;
                return;
            }
        }

        switch (state) {
            case LEFT:
            case RIGHT:
                c->speed.x *= -1;
                c->response(SIMPLE_DELTA);
                break;
            case DOWN:
            case UP:
              c->speed.y *= -1;
              c->response(SIMPLE_DELTA);
              break;
            case NONE:
              break;
        }
    }
}

void MapRules::rule(GameRuler *gr) {

}

void PlayerRules::rule(GameRuler *gr) {
    std::cout << "score: "<< gr->score << std::endl;
    if (!(lives))
        gr->endGame();
    //for (Bonus * b : gr->bonuses) {
    //    if (isCollisionBonusPlayer(b, gr->player, gr) != NONE) {
    //        gr->rules.insert(make_pair(std::to_string(rand() % RAND_MAX), b->effect()));
    //        gr->bonuses.erase(std::find(gr->bonuses.begin(), gr->bonuses.end(), b));
    //    }
    //}

    for (int i = 0; i < gr->bonuses.size(); i++) {
        Bonus * b = gr->bonuses[i];
        if (isCollisionBonusPlayer(b, gr->player, gr) != NONE) {
            std::string name = std::to_string(rand() % RAND_MAX);
            Rule * rule = b->effect();
            rule->ruleName = name;
            gr->rules.insert(make_pair(name, rule));
            gr->bonuses.erase(std::find(gr->bonuses.begin(), gr->bonuses.end(), b));
            delete b;
        }

    }
}

void SimpleRule::rule(GameRuler *gr) {
    //do nothing
}

SimpleRule::~SimpleRule() {

}

void StartRule::rule(GameRuler *gr)  {
    gr->circles[ind]->pos.x = gr->player->pos.x;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
        float speedx = rand() % CIRCLE_SPEED;
        float speedy = -((CIRCLE_SPEED - speedx)+50);
        gr->circles[ind]->speed = Vector2f(speedx, speedy);
        gr->circles[ind]->response(SIMPLE_DELTA);
        delete gr->rules[ruleName];
        gr->rules.erase(ruleName);
    }
}

StartRule::StartRule(int ind, std::string name)  : ind(ind) {
    ruleName = name;
};

void AddCircleRule::rule(GameRuler *gr) {
    Vector2f winSize = gr->winSize;
    std::string n = std::to_string(rand() % RAND_MAX);
    gr->circles.push_back(new Circle(CIRCLE_START_PARAM));
    gr->rules.insert(make_pair(n, new StartRule(gr->circles.size()-1, n)));
    delete gr->rules[ruleName];
    gr->rules.erase(ruleName);
}

void SpeedUpRule::rule(GameRuler *gr) {
    for (Circle * c : gr->circles) {
        c->speed.x *= CIRCLE_SPEEDUP_VALUE;
        c->speed.y *= CIRCLE_SPEEDUP_VALUE;
    }
    delete gr->rules[ruleName];
    gr->rules.erase(ruleName);
}

void SizeUpRule::rule(GameRuler *gr) {
    gr->player->resize({2, 1});


    delete gr->rules[ruleName];
    gr->rules.erase(ruleName);
}

void TrampolineRule::rule(GameRuler *gr) {
    for (Circle * c : gr->circles) {
        if (c->pos.y + c->R > gr->winSize.y - TRAMPOLINE_POS_Y) {
            c->speed.y *= -1;
            c->response(SIMPLE_DELTA);
            delete gr->rules[ruleName];
            gr->rules.erase(ruleName);
        }
    }
}

void AddLiveRule::rule(GameRuler *gr) {
    ((PlayerRules *)gr->rules["player"])->lives++;
    delete gr->rules[ruleName];
    gr->rules.erase(ruleName);
}
