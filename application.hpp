//
// Created by Екатерина Заболотских on 09/06/2019.
//

#ifndef ARCANOID_APPLICATION_HPP
#define ARCANOID_APPLICATION_HPP

#include <SFML/Graphics.hpp>

#include "screen.hpp"

using namespace sf;

class Application {
    Vector2f winSize;
    RenderWindow * sfmlWin;
    Screen * screen;
public:
    Application(Vector2f winSize, std::string const & title);
    void setScreen(Screen * screen);
    void run();
    void render();
    void response(float delta);
    Vector2f const & getWinSize() { return winSize; }
};

#endif //ARCANOID_APPLICATION_HPP

