//
// Created by Екатерина Заболотских on 09/06/2019.
//

#include <iostream>
#include "application.hpp"


Application::Application(Vector2f winSize, std::string const &title) : winSize(winSize) {

    sfmlWin = new RenderWindow(sf::VideoMode(winSize.x, winSize.y), "arcanoid");
    sfmlWin->setVerticalSyncEnabled(true);
    sfmlWin->setFramerateLimit(60);

}
void Application::run() {
    Clock clock;
    while (sfmlWin->isOpen()) {

        sf::Event e;
        while (sfmlWin->pollEvent(e)) {

            switch (e.type) {
                case sf::Event::EventType::Closed:
                    sfmlWin->close();
                    break;
                default:
                    break;
            }
        }

        float delta = ((float)(clock.getElapsedTime().asMicroseconds() / 1000000.0));
        clock.restart();
        //std::cout << delta << std::endl;

        sfmlWin->clear(sf::Color::White);
        response(delta);
        render();
        sfmlWin->display();
    }
}
void Application::render() {
    screen->render(sfmlWin);
}
void Application::response(float delta) {
    screen->response(delta);
}
void Application::setScreen(Screen * screen) {
    this->screen = screen;
}