#include <iostream>
#include "entity/object.hpp"
#include "entity/brick.hpp"
#include "application.hpp"
#include "game/GameScreen.hpp"

using namespace sf;
using namespace std;


int main() {
    Application app({1000, 1000}, "game");
    ScreenGame game(app.getWinSize());
    app.setScreen(&game);
    app.run();
    return 0;
}