//
// Created by Екатерина Заболотских on 09/06/2019.
//

#ifndef ARCANOID_SCREENGAME_HPP
#define ARCANOID_SCREENGAME_HPP


#include <SFML/Graphics.hpp>
#include "entity/circle.hpp"

using namespace sf;





class Screen {
protected:
    Vector2f winSize;
public:
    Screen(Vector2f winSize) : winSize(winSize) {}
    virtual void render(RenderWindow * win) = 0;
    virtual void response(float delta) = 0;
    virtual ~Screen() {};
};



class ScreenMenu: public  Screen {
    ScreenMenu(Vector2f winSize) : Screen(winSize) {}
    void render(RenderWindow * win) override;
    void response(float delta) override;
    ~ScreenMenu() override {}
};

#endif //ARCANOID_SCREENGAME_HPP
