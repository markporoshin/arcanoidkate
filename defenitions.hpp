//
// Created by Марк on 27.06.19.
//

#ifndef ARCANOID_DEFENITIONS_HPP
#define ARCANOID_DEFENITIONS_HPP


#define PLAYER_START_SIZE {winSize.x / 7, winSize.y / 40}
#define PLAYER_START_SPEED {winSize.x / 2, 0}
#define PLAYER_START_POS {winSize.x / 2, winSize.y - winSize.y / 80}
#define PLAYER_START_PARAM PLAYER_START_POS, PLAYER_START_SPEED, PLAYER_START_SIZE

#define CIRCLE_START_RADIUS winSize.x / 40
#define CIRCLE_START_SPEED {0, 0}
#define CIRCLE_START_POS {winSize.x / 2, winSize.y - winSize.y / 20}
#define CIRCLE_START_PARAM CIRCLE_START_POS, CIRCLE_START_RADIUS, CIRCLE_START_SPEED

#define PLAYER_START_LIVES 1

#define E 0.01f //accurancy
#define CIRCLE_SPEED 500
#define SIMPLE_DELTA 0.015f

#define BRICK_MARGIN 10

#define BRICK_COST 1
#define DIFHARDNESS_BRICK_COST 1
#define FLICKERING_BRICK_COST 1

#define CIRCLE_SPEEDUP_VALUE 2

//Trampoline
#define TRAMPOLINE_POS_Y 10

#endif //ARCANOID_DEFENITIONS_HPP
