//
// Created by Екатерина Заболотских on 10/06/2019.
//

#include "GameScreen.hpp"



ScreenGame::ScreenGame(Vector2f winSize) : Screen(winSize) {
    gr = new GameRuler(winSize);
}

void ScreenGame::response(float delta) {
    if (gr->game) {
        gr->player->response(delta);
        gr->map->response(delta);
        for (Bonus * b : gr->bonuses)
            b->response(delta);
        for (Circle * c : gr->circles)
            c->response(delta);
        gr->update(delta);
    } else {
        gr->endGame();
    }

}

void ScreenGame::render(RenderWindow * win) {
    if (gr->game) {
        gr->player->render(win);
        gr->map->render(win);
        for (Bonus * b : gr->bonuses)
            b->render(win);
        for (Circle * c : gr->circles)
            c->render(win);
    }
}

ScreenGame::~ScreenGame() {
    delete gr;
}