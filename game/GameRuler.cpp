//
// Created by Екатерина Заболотских on 10/06/2019.
//
#include <iostream>
#include "GameRuler.hpp"

#include "../defenitions.hpp"




using namespace std;


GameRuler::GameRuler(Vector2f winSize) : winSize(winSize), score(0), game(true) {
    player = new Player(PLAYER_START_PARAM);
    circles.push_back(new Circle(CIRCLE_START_PARAM));

    rules.insert(make_pair("player", new PlayerRules(PLAYER_START_LIVES)));
    rules.insert(make_pair("circle", new CircleRules()));
    rules.insert(make_pair("map", new MapRules()));
    rules.insert(make_pair("start", new StartRule(0)));

    map = new Map("level1.txt", winSize);
}

void GameRuler::endGame() {
    game = false;

    //delete player;
    //delete map;
    //for (auto * c : circles)
    //    delete c;
    //for (auto r : rules)
    //    delete r.second;
    std::cout << "end game" << std::endl;
}


void GameRuler::update(float delta) {
    for (pair<string, Rule *> p : rules)
        p.second->rule(this);
}

GameRuler::~GameRuler() {
    delete player;
    delete map;
    for (Circle * c : circles)
        delete c;
    for (Bonus * b : bonuses)
        delete b;
    for (pair<string, Rule *> p : rules)
        delete p.second;

}