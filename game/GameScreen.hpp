//
// Created by Екатерина Заболотских on 10/06/2019.
//

#ifndef ARCANOID_GAMESCREEN_HPP
#define ARCANOID_GAMESCREEN_HPP


#include "../entity/circle.hpp"
#include "../screen.hpp"
#include "GameRuler.hpp"

class ScreenGame: public Screen {
    GameRuler * gr;
public:
    ScreenGame(Vector2f winSize);
    void render(RenderWindow * win) override;
    void response(float delta) override;
    ~ScreenGame() override;
};

#endif //ARCANOID_GAMESCREEN_HPP
