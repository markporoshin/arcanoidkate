//
// Created by Екатерина Заболотских on 10/06/2019.
//



#ifndef ARCANOID_GAMERULER_HPP
#define ARCANOID_GAMERULER_HPP

#pragma once

#include <SFML/Graphics.hpp>
#include <map>
#include "../entity/map.hpp"
#include "../entity/player.hpp"
#include "../entity/circle.hpp"
#include "../entity/bonus.hpp"

using namespace sf;
using namespace std;

class Rule;

class GameRuler {
public:
    bool game;
    int score;


    Vector2f winSize;

    Map * map;
    Player * player;
    vector<Circle *> circles;
    vector<Bonus *> bonuses;
    std::map<string, Rule *> rules;

    GameRuler(Vector2f winSize);

    void update(float delta);

    void endGame();

    ~GameRuler();
};


#endif //ARCANOID_GAMERULER_HPP
