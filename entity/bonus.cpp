//
// Created by Екатерина Заболотских on 10/06/2019.
//

#include <fstream>
#include "bonus.hpp"


#define SIMPLE_BONUS_SPEED {0, 200}
#define SIMPLE_BONUS_R 20

using namespace std;
using namespace sf;

typedef enum {eSimple, bonusCircle, speedupBonus, sizeupBonus, trampolineBonus, lifeBonus, unknown} string_code;
string_code hashstr(string const & str) {
    if(str == "simple_bonus") return eSimple;
    else if(str == "circle_bonus") return bonusCircle;
    else if(str == "speedup_bonus") return speedupBonus;
    else if(str == "sizeup_bonus") return sizeupBonus;
    else if(str == "trampoline_bonus") return trampolineBonus;
    else if(str == "addLife_bonus") return lifeBonus;
    return unknown;
}
Bonus * parseBonus(std::ifstream & in) {
    std::string type;
    in >> type;
    switch (hashstr(type)) {
        case eSimple:
            return new SimpleBonus({0, 0}, SIMPLE_BONUS_SPEED, nullptr);
        case bonusCircle:
            return new AddCircleBonus({0, 0}, SIMPLE_BONUS_SPEED, nullptr);
        case speedupBonus:
            return new CircleSpeedUpBonus({0, 0}, SIMPLE_BONUS_SPEED, nullptr);
        case sizeupBonus:
            return new PlayerSizeUpBonus({0, 0}, SIMPLE_BONUS_SPEED, nullptr);
        case trampolineBonus:
            return new TrampolineBonus({0, 0}, SIMPLE_BONUS_SPEED, nullptr);
        case lifeBonus:
            return new LifeBonus({0, 0}, SIMPLE_BONUS_SPEED, nullptr);
    }
    return nullptr;
}

Bonus::Bonus(Vector2f pos, Vector2f speed, Brick *owner) : Movable(pos, speed), owner(owner), isFree(false) {
    //TODO dependent on brick size
    R = SIMPLE_BONUS_R;

    shape = new CircleShape(R);
    shape->setPosition(pos);
    shape->setOrigin(R, R);
    shape->setFillColor(Color::Black);
}
void Bonus::free() {
    isFree = true;
}
void Bonus::response(float deltaTime) {
    if (isFree)
        Movable::response(deltaTime);
}
void Bonus::render(RenderWindow *win) {
    if (!isFree)
        pos = owner->pos;
    Object::render(win);
}



Rule * Bonus::effect() {
    return new SimpleRule();
}
Rule * SimpleBonus::effect() {
    return new SimpleRule();
}

AddCircleBonus::AddCircleBonus(Vector2f pos, Vector2f speed, Brick *owner) : Bonus(pos, speed, owner) {
    shape->setFillColor(Color::Yellow);
}
Rule * AddCircleBonus::effect() {
    return new AddCircleRule();
}

CircleSpeedUpBonus::CircleSpeedUpBonus(Vector2f pos, Vector2f speed, Brick *owner) : Bonus(pos, speed, owner) {
    shape->setFillColor(Color::Green);
}
Rule * CircleSpeedUpBonus::effect() {
    return new SpeedUpRule();
}


PlayerSizeUpBonus::PlayerSizeUpBonus(Vector2f pos, Vector2f speed, Brick *owner) : Bonus(pos, speed, owner) {
    shape->setFillColor(Color::Red);
}
Rule * PlayerSizeUpBonus::effect() {
    return new SizeUpRule();
}

TrampolineBonus::TrampolineBonus(Vector2f pos, Vector2f speed, Brick *owner) : Bonus(pos, speed, owner) {
    shape->setFillColor(Color::Blue);
}
Rule * TrampolineBonus::effect() {
    return new TrampolineRule();
}

LifeBonus::LifeBonus(Vector2f pos, Vector2f speed, Brick *owner) : Bonus(pos, speed, owner) {
    shape->setFillColor(Color::Magenta);
}
Rule * LifeBonus::effect() {
    return new AddLiveRule();
}