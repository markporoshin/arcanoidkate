//
// Created by Екатерина Заболотских on 09/06/2019.
//

#ifndef ARCANOID_CIRCLE_HPP
#define ARCANOID_CIRCLE_HPP

#pragma once

#include "object.hpp"

class Circle : public Movable {

public:
    Shape * center;
    float R;

    Circle(Vector2f const & pos, float R, Vector2f speed = {0, 0});

    void response(float deltaTime) override;
    void render(RenderWindow *win) override;
};

#endif //ARCANOID_CIRCLE_HPP
