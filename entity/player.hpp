//
// Created by Екатерина Заболотских on 10/06/2019.
//

#ifndef ARCANOID_PLAYER_HPP
#define ARCANOID_PLAYER_HPP

#pragma once

#include "object.hpp"

class Player : public Movable {
public:
    Shape * center;
    Vector2f size;
    Player(Vector2f pos, Vector2f speed, Vector2f size);

    void response(float deltaTime) override;

    void render(RenderWindow *win) override;

    void resize(Vector2f const & scale) override;
};


#endif //ARCANOID_PLAYER_HPP
