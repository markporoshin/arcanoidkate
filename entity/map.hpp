//
// Created by Екатерина Заболотских on 10/06/2019.
//

#ifndef ARCANOID_MAP_HPP
#define ARCANOID_MAP_HPP

#pragma once

#include "object.hpp"
#include "brick.hpp"


using namespace std;

class Map : public Movable {
    int N, M;           //Map size
public:
    vector<Brick *> map;
    Map(string const &, Vector2f winSize);

    void render(RenderWindow *win) override;

    void response(float deltaTime) override;

    class MapLoader {
    public:
        static pair<vector<Brick *>, pair<int, int>> loadMap(string const &, Vector2f winSize);
    };
};


#endif //ARCANOID_MAP_HPP
