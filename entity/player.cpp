//
// Created by Екатерина Заболотских on 10/06/2019.
//

#include "player.hpp"

using namespace sf;


Player::Player(Vector2f pos, Vector2f speed, Vector2f size)  : Movable(pos, speed), size(size) {
    shape = new RectangleShape(size);
    shape->setFillColor(sf::Color::Black);
    shape->setOrigin(size.x / 2, size.y / 2);

    center = new CircleShape(3);
    center->setPosition(pos.x, pos.y);
    center->setOrigin(1.5, 1.5);
    center->setFillColor(Color::Red);
}

void Player::response(float deltaTime) {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
        Movable::response(-deltaTime);
    } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
        Movable::response(deltaTime);
    }
}

void Player::render(RenderWindow *win) {
    Object::render(win);
    center->setPosition(pos.x, pos.y);
    win->draw(*center);
}

void Player::resize(Vector2f const & scale) {
    shape->setScale(scale);
}
