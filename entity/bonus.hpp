//
// Created by Екатерина Заболотских on 10/06/2019.
//



#ifndef ARCANOID_BONUS_HPP
#define ARCANOID_BONUS_HPP

#pragma once


#include "../rules/rules.hpp"
#include "object.hpp"
#include "brick.hpp"

Bonus * parseBonus(std::ifstream & in);


class Bonus : public Movable {
protected:
    bool isFree;
public:
    float R;
    Brick * owner;
    Bonus(Vector2f pos, Vector2f speed, Brick * owner);
    virtual Rule * effect() = 0;

    virtual void free();

    void response(float deltaTime) override;
    void render(RenderWindow *win) override;
    virtual ~Bonus() {};
};

class SimpleBonus : public Bonus {
public:
    SimpleBonus(Vector2f pos, Vector2f speed, Brick * owner) : Bonus(pos, speed, owner) {};
    Rule * effect() override;
};

class AddCircleBonus : public Bonus {
public:
    AddCircleBonus(Vector2f pos, Vector2f speed, Brick *owner);

    Rule *effect() override;
};

class CircleSpeedUpBonus : public Bonus {
public:
    CircleSpeedUpBonus(Vector2f pos, Vector2f speed, Brick *owner);

    Rule *effect() override;
};

class PlayerSizeUpBonus : public Bonus {
public:
    PlayerSizeUpBonus(Vector2f pos, Vector2f speed, Brick * owner);

    Rule *effect() override;
};

class TrampolineBonus : public Bonus {
public:
    TrampolineBonus(Vector2f pos, Vector2f speed, Brick * owner);

    Rule *effect() override;
};

class LifeBonus : public Bonus {
public:
    LifeBonus(Vector2f pos, Vector2f speed, Brick * owner);

    Rule *effect() override;
};
#endif //ARCANOID_BONUS_HPP
