//
// Created by Екатерина Заболотских on 08/06/2019.
//

#include "object.hpp"

void Object::render(RenderWindow *win) {
    shape->setPosition(pos);
    win->draw(*shape);
}
void Movable::response(float delta) {
    pos += delta * speed;
}