//
// Created by Екатерина Заболотских on 09/06/2019.
//

#ifndef ARCANOID_BRICK_HPP
#define ARCANOID_BRICK_HPP

#pragma once

#include "object.hpp"
class Bonus;

class Brick : public Movable {
public:
    int cost;
    bool alive;
    Bonus * bonus;
    Vector2f size;
    Brick(Vector2f pos, Vector2f size);
    void render(RenderWindow *win) override;
    virtual Bonus * handleHit();
};


class UnbrokenBrick : public Brick {
public:
    UnbrokenBrick(Vector2f pos, Vector2f size);
    Bonus *handleHit() override;
};


class DifhardnessBrick : public Brick {
    int lives;
public:
    DifhardnessBrick(Vector2f pos, Vector2f size, int lives);
    Bonus *handleHit() override;
};


class FlickeringBrick : public Brick {
    bool totalAlive;
    float period;
    float curTime;
    int dir;
public:
    FlickeringBrick(Vector2f pos, Vector2f size, float period);

    void response(float deltaTime) override;

    Bonus *handleHit() override;
};
#endif //ARCANOID_BRICK_HPP
