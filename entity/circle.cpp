//
// Created by Екатерина Заболотских on 09/06/2019.
//

#include <iostream>
#include "circle.hpp"

using namespace sf;

Circle::Circle(Vector2f const &pos, float R, Vector2f speed) : Movable(pos, speed), R(R) {
    shape = new CircleShape(R);
    shape->setPosition(pos);
    shape->setOrigin(R, R);
    shape->setFillColor(Color::Black);

    center = new CircleShape(3);
    center->setPosition(pos);
    center->setOrigin(1.5, 1.5);
    center->setFillColor(Color::Red);
}
void Circle::response(float delta) {
    Movable::response(delta);
}
void Circle::render(RenderWindow *win) {
    shape->setPosition(pos.x, pos.y);
    center->setPosition(pos.x, pos.y);
    win->draw(*shape);
    win->draw(*center);
    //std::cout << "pos: " << pos.x << " " << pos.y << std::endl;
}
