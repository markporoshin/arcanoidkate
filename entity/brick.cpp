//
// Created by Екатерина Заболотских on 09/06/2019.
//

#include <fstream>
#include <iostream>
#include "brick.hpp"
#include "map.hpp"
#include "bonus.hpp"
#include "../defenitions.hpp"

using namespace sf;

static Vector2f countBrickPos(Vector2f winSize, int N, int M, int i, int j) {
    return {(i+0.5f) * winSize.x / M, (j+0.5f) * winSize.y / N / 2};
}
static Vector2f countBrickSize(Vector2f winSize, int N, int M) {
    return {winSize.x / M - BRICK_MARGIN, (winSize.y / N - BRICK_MARGIN) / 2};
}
Brick * parseBrick(ifstream & in, Vector2f winSize, int N, int M) {
    char type;
    int i, j;
    in >> type >> i >> j;
    switch (type) {
        case 'B':
            return new Brick(countBrickPos(winSize, N, M, i, j), countBrickSize(winSize, N, M));
        case 'D':
            int lives;
            in >> lives;
            return new DifhardnessBrick(countBrickPos(winSize, N, M, i, j), countBrickSize(winSize, N, M), lives);
        case 'U':
            return new UnbrokenBrick(countBrickPos(winSize, N, M, i, j), countBrickSize(winSize, N, M));
        case 'F':
            float period;
            in >> period;
            return new FlickeringBrick(countBrickPos(winSize, N, M, i, j), countBrickSize(winSize, N, M), period);
    }
    return nullptr;
}
pair<vector<Brick *>, pair<int, int>> Map::MapLoader::loadMap(string const & levelMap, Vector2f winSize) {
    vector<Brick *> map;
    int N, M, rows;
    ifstream levelMapIn(levelMap);

    levelMapIn >> N >> M >> rows;
    for (int i = 0; i < rows; ++i) {
        Brick * brick = parseBrick(levelMapIn, winSize, N, M);
        Bonus * bonus = parseBonus(levelMapIn);
        brick->bonus = bonus;
        if (bonus)
            bonus->owner = brick;
        map.push_back(brick);
    }
    return {map, {N, M}};
}

Brick::Brick(Vector2f pos, Vector2f size) : Movable(pos, {0, 0}), size(size), bonus(nullptr), alive(true) {
    shape = new RectangleShape(size);
    shape->setPosition(pos);
    shape->setOrigin(size.x / 2, size.y / 2);
    shape->setOutlineColor(sf::Color::Black);
    shape->setFillColor(sf::Color::White);
    shape->setOutlineThickness(3);
    cost = BRICK_COST;
}

void Brick::render(RenderWindow *win) {
    if (alive) {
        Object::render(win);
        if (bonus)
            this->bonus->render(win);
    }
}
Bonus * Brick::handleHit() {
    alive = false;
    return bonus;
}








UnbrokenBrick::UnbrokenBrick(Vector2f pos, Vector2f size) : Brick(pos, size) {
    shape->setFillColor(sf::Color::Black);
}
Bonus *UnbrokenBrick::handleHit() {
    return bonus;
}

DifhardnessBrick::DifhardnessBrick(Vector2f pos, Vector2f size, int lives) : Brick(pos, size), lives(lives) {
    shape->setFillColor(sf::Color(200, 200, 200));
    cost = DIFHARDNESS_BRICK_COST;
}
Bonus *DifhardnessBrick::handleHit() {
    if(!lives--) return Brick::handleHit();
    return nullptr;
}

FlickeringBrick::FlickeringBrick(Vector2f pos, Vector2f size, float period) : Brick(pos, size), period(period) {
    shape->setOutlineColor(sf::Color::Magenta);
    curTime = 0;
    dir = 1;
    cost = FLICKERING_BRICK_COST;
    totalAlive = true;
}

void FlickeringBrick::response(float deltaTime) {
    if (totalAlive) {
        curTime += dir * deltaTime;
        std::cout << curTime << std::endl;
        if (curTime > period) {
            dir = -1;
            alive = true;
        } else if (curTime < 0) {
            dir = 1;
            alive = false;
        }
        Movable::response(deltaTime);
    }
}

Bonus *FlickeringBrick::handleHit() {
    totalAlive = false;
    alive = false;
    return Brick::handleHit();
}


