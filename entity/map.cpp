//
// Created by Екатерина Заболотских on 10/06/2019.
//

#include "map.hpp"

using namespace sf;

Map::Map(string const & mapDescription, Vector2f winSize) : Movable({0, 0}, {0, 0}) {
    auto data = MapLoader::loadMap(mapDescription, winSize);
    map = data.first;
    N = data.second.first;
    N = data.second.second;
}

void Map::response(float deltaTime) {
    for (Brick * b : map) {
        b->response(deltaTime);
    }
}

void Map::render(RenderWindow *win) {
    for (Brick * b : map) {
        b->render(win);
    }
}
