//
// Created by Екатерина Заболотских on 08/06/2019.
//

#ifndef ARCANOID_OBJECT_HPP
#define ARCANOID_OBJECT_HPP

#pragma once

#include <SFML/Graphics.hpp>

using namespace sf;

class Object {
protected:
    Shape * shape;
public:


    Vector2f pos;
    Object(Vector2f pos) : pos(pos) {}
    virtual void render(RenderWindow * win);
    virtual ~Object() { delete shape; }
    virtual void resize(Vector2f const & scale) {};
};


class Movable : public Object {
public:
    Vector2f speed;
    Movable(Vector2f pos, Vector2f speed) : Object(pos), speed(speed) {}
    virtual void response(float deltaTime);
};


#endif //ARCANOID_OBJECT_HPP
